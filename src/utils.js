import {Settings} from './settings';

let CACHE_TEMPLATES = new Map();

/** Get is our ajax caller implemented as a promise
 * From Jake Archibald's Promises and Back:
 * http://www.html5rocks.com/en/tutorials/es6/promises/#toc-promisifying-xmlhttprequest
*/
/**
 * 
 * @param {string} url 
 * @param {string} method 
 * @param {string} data 
 * @returns {json} 
 */
function get(url, method = 'GET', data = null) {
    // Return a new promise.
    return new Promise((resolve, reject)=> {
        if (CACHE_TEMPLATES.has(url)) {
            resolve(CACHE_TEMPLATES.get(url));
        }else {
            // Do the usual XHR stuff
            var req = new XMLHttpRequest();

            req.open(method, Settings.baseURL + url, true);
    
            req.onload = ()=> {
                // This is called even on 404 etc
                // so check the status
                if (req.status == 200) {
                    // Resolve the promise with the response text
                    CACHE_TEMPLATES.set(url, req.response);
                    resolve(req.response);
                }
                else {
                // Otherwise reject with the status text
                // which will hopefully be a meaningful error
                    //reject(Error(req.statusText));
                }
            };
            // Handle network errors
            req.onerror = ()=> {
                reject(Error("Network Error"));
            };
            //The content type from the post
            //req.setRequestHeader("Content-Type", "application/json; charset=utf8");

            if(data){
                console.log(data);
                req.send(data);
            }else {
                // Make the request
                req.send();
            }
        }
    });
}
/**
 * 
 * @param {String} cname 
 * @param {String} cvalue 
 * @param {String} exdays 
 * @function
 */
function setCookie(cname, cvalue, exdays) {
    if (cvalue && cvalue !== '') {
      let d = new Date();
      d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
      let expires = 'expires=' + d.toUTCString();
      document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
    }
}
/**
 * 
 * @param {String} cname 
 * @function
 */
function getCookie(cname) {
    try {
        let exp = new RegExp(cname + '[\\s]*=[\\s]*([\\w]*)');
        return document.cookie.match(exp)[1];
    }catch(e){
        throw "";
    }    
}

export {get, getCookie, setCookie};
