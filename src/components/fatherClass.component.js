class Father {
    /**
     * Father component
     * @class
     * @param {String} mark 
     */
    constructor(mark) {
        let selectedTarget; 
        try{
            //Here I check if the element mark exists in opposite case I throw an error
            selectedTarget = document.querySelector('#' + mark);
            if(selectedTarget) {
                return true;
            }else {
                throw("Error. Selected output target for component " + this.constructor.name + " doesn't exist");     
            }
        //If something goes wrong I show an error
        }catch(err){
            if(selectedTarget){
                selectedTarget.innerHTML = "Problems rendering " + this.constructor.name+" -> "+err;
            }
            throw err;
        };  
    }
}

export default Father;
