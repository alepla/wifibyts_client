//HOME COMPONENT
import {slider} from './sliderCtrl';
import Father from './fatherClass.component';
import {getCookie} from '../utils';
/**
 * Creates homeCtrl.
 * @class
 * @returns {controller}
 */
class homeCtrl extends Father {    
    constructor(res, tar, texts, mark) {
        super(mark);
         //Select the lang in the coockies
         let lang = getCookie("app-lang");
         //If lang is null then select the default ES
         if (lang == null) lang = "es";
         //Filtering the texts by language
         const resultEn = res.filter(text => text.lang.includes("EN") || text.lang.includes("en"));
         const resultEs = res.filter(text => text.lang.includes("ES") || text.lang.includes("es"));
         const resultVa = res.filter(text => text.lang.includes("VA") || text.lang.includes("va"));
         //Sending the data to the template depending the selected language
         if(lang == "es"){
             //If resultEs is diferent to 0 I show the data else show an advise message
             if(resultEs.length != 0){
                 this.render(resultEs, tar, mark);
                 document.getElementById("messageNoResultsTexts").style.display  = 'none';
             }else {
                 this.render(resultEs, tar, mark);
                 document.getElementById("messageNoResultsTexts").style.display  = '';
             }
             
         }else if(lang == "en"){
             //If resultEn is diferent to 0 I show the data else show an advise message
             if(resultEn.length != 0){
                 this.render(resultEn, tar, mark);
                 document.getElementById("messageNoResultsTexts").style.display  = 'none';
             }else {
                 this.render(resultEn, tar, mark);
                 document.getElementById("messageNoResultsTexts").style.display  = '';
             }
         }else if(lang == "vl"){
             //If resultVa is diferent to 0 I show the data else show an advise message
             if(resultVa.length != 0){
                 this.render(resultVa, tar, mark);
                 document.getElementById("messageNoResultsTexts").style.display  = 'none';
             }else {
                 this.render(resultVa, tar, mark);
                 document.getElementById("messageNoResultsTexts").style.display  = '';
             }
         }
        if(texts != undefined) {
            //Filtering the texts by language
            const resultEn = texts.textos.filter(text => text.lang.includes("EN") || text.lang.includes("en"));
            const resultEs = texts.textos.filter(text => text.lang.includes("ES") || text.lang.includes("es"));
            const resultVa = texts.textos.filter(text => text.lang.includes("VA") || text.lang.includes("va"));
            //Sending the data to the template depending the selected language
            if(lang == "es"){                
                if(resultEs.length >= 3){
                    slider(resultEs);
                    document.getElementById("messageNoResults").style.display  = 'none';
                    document.getElementById("sliderNoResults").style.display  = '';
                }else {
                    document.getElementById("messageNoResults").style.display  = '';
                    document.getElementById("sliderNoResults").style.display  = 'none';
                }
            }else if(lang == "en"){
                if(resultEn.length >= 3){
                    slider(resultEn);
                    document.getElementById("messageNoResults").style.display  = 'none';
                    document.getElementById("sliderNoResults").style.display  = '';
                }else {
                    document.getElementById("messageNoResults").style.display  = '';
                    document.getElementById("sliderNoResults").style.display  = 'none';
                }
            }else if(lang == "vl"){
                if(resultVa.length >= 3){
                    slider(resultVa);
                    document.getElementById("messageNoResults").style.display  = 'none';
                    document.getElementById("sliderNoResults").style.display  = '';
                }else {
                    document.getElementById("messageNoResults").style.display  = '';
                    document.getElementById("sliderNoResults").style.display  = 'none';
                }
            }
        }
    }

    render(res, tar, mark) {
        const markup = `
            <div class="home">
                <p id="messageNoResults" data-tr="No results in that language" class="noResults">No results in that language</p><br/>
                <div class="slider" id="sliderNoResults">
                    <div class="slider--text" id="sliderText"></div>
                    <div id="sliderMedia"></div>
                    <a class="prev" id="prev">&#10094;</a>
                    <a class="next" id="next">&#10095;</a>
                    <div style="text-align:center">
                        <span class="dot " id="dotLeft"></span> 
                        <span class="dot " id="dotCenter"></span> 
                        <span class="dot " id="dotRight"></span> 
                    </div>
                    <div style="text-align:center">
                        <a class="play" id="play">&#9655;</a>
                        <a class="stop" id="stop">&#9632;</a>
                    </div>
                </div>
                <div class="tarifas">
                    <h2 class="tarifas--title" data-tr="Best Rates">Best Rates</h2>
                    <div class="tarifas--content">
                        <div id="tarifa" class="tarifa--father"></div>
                    </div>
                </div>
                <p id="messageNoResultsTexts" data-tr="No results in that language" class="noResults">No results in that language</p><br/>
                <div id="texts"></div>
            </div>
        `;
        document.getElementById(mark).innerHTML = markup;

        res.forEach((x) => {
            const texts = `
                <h2 class="home--title">${x.titulo}</h2>
                <div class="home--textos">
                    <div class="home--texto">
                        <h3>${x.caja_derecha_titulo}</h3>
                        ${x.caja_derecha_texto}
                    </div>
                    <div class="home--texto">
                        <h3>${x.caja_izquierda_titulo}</h3>
                        ${x.caja_izquierda_texto}
                    </div>
                </div>
            `;
            document.getElementById("texts").innerHTML = texts;
        });

        tar.forEach((x) => {
            const tarifas = `
                <div class="tarifa--child">
                    <h2 class="tarifa--titlo"><img class="tarifa--logo" src="${x.logo}" /> ${x.nombretarifa}</h2>
                    <h2 class="tarifa--precio">${x.precio}€/<span data-tr="month">mes</span></h2>
                    <div class="tarifa--subtarifas">
                        <div id="subtarifas${x.codtarifa}"></div>
                    </div>
                    <div class="tarifa--btns">
                        <button type="button" class="tarifa--btn" data-tr="MORE INFO.">MÁS INFO.</button>
                        <button type="button" class="tarifa--btn" data-tr="CONTRACT">CONTRATAR</button>
                    </div>
                </div>
            `;
            document.getElementById("tarifa").innerHTML += tarifas;
        });

        //forEach to list all the rates
        tar.forEach((x) => {

            x.subtarifas.forEach((y) => {
                //If subrate is equal to 1 show it
                if(y.tipo_tarifa == 1) {

                    const subtarifaMóvil = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/ios/50/000000/touchscreen-smartphone.png">
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb up">mb subida</span></p>
                            <p>${y.subtarifa_velocidad_conexion_bajada}<span data-tr="mb down">mb bajada</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaMóvil;
                //If subrate is equal to 2 show it
                }else if(y.tipo_tarifa == 2){

                    const subtarifaFijo = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/ios/50/000000/phone.png">
                            <p data-tr="Unlimited minutes">Minutos ilimitados</p>
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb up">mb subida</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaFijo;
                //If subrate is equal to 3 show it
                }else if(y.tipo_tarifa == 3){

                    const subtarifaFibra = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/metro/50/000000/network-cable.png">
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb fiber optic rise">mb subida fibra óptica</span></p>
                            <p>${y.subtarifa_velocidad_conexion_bajada}<span data-tr="mb fiber optic down">mb bajada fibra óptica</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaFibra;
                //If subrate is equal to 4 show it
                }else if(y.tipo_tarifa == 4){

                    const subtarifaWifi = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/metro/50/000000/wifi.png">
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb up">mb subida</span></p>
                            <p>${y.subtarifa_velocidad_conexion_bajada}<span data-tr="mb down">mb bajada</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaWifi;
                //If subrate is equal to 5 show it
                }else if(y.tipo_tarifa == 5){

                    const subtarifaTV = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/ios/50/000000/tv.png">
                            <p>${y.subtarifa_num_canales} <span data-tr="free chanels">canales gratis</span></p>
                            <p data-tr="Free futbol package">Paquete fútbol gratis</p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaTV;

                }

            });
        });
    }
}

export default homeCtrl;