//TARIFAS COMPONENT
import Father from './fatherClass.component';
/**
 * Creates tarifasCtrl.
 * @class
 * @returns {controller}
 */
class tarifasCtrl extends Father {
    constructor(res, mark) {
        super(mark);
        this.render(res, mark);
    }

    render(res, mark) {
        const markup = `
            <div class="tarifas">
                <h2 class="tarifas--title" data-tr="Mobil rates">Tarifas moviles</h2>
                <div class="tarifas--content">
                    <div id="tarifa" class="tarifa--father"></div>
                </div>
            </div>
        `;
        document.getElementById(mark).innerHTML = markup;

        res.forEach((x) => {
            //The default html
            const tarifas = `
                <div class="tarifa--child">
                    <h2 class="tarifa--titlo"><img class="tarifa--logo" src="${x.logo}" /> ${x.nombretarifa}</h2>
                    <h2 class="tarifa--precio">${x.precio}€/<span data-tr="month">mes</span></h2>
                    <div class="tarifa--subtarifas">
                        <div id="subtarifas${x.codtarifa}"></div>
                    </div>
                    <div class="tarifa--btns">
                        <button type="button" class="tarifa--btn" data-tr="MORE INFO.">MÁS INFO.</button>
                        <button type="button" class="tarifa--btn" data-tr="CONTRACT">CONTRATAR</button>
                    </div>
                </div>
            `;
            document.getElementById("tarifa").innerHTML += tarifas;
        });
        //forEach to list all the rates
        res.forEach((x) => {

            x.subtarifas.forEach((y) => {
                //If subrate is equal to 1 show it
                if(y.tipo_tarifa == 1) {

                    const subtarifaMóvil = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/ios/50/000000/touchscreen-smartphone.png">
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb up">mb subida</span></p>
                            <p>${y.subtarifa_velocidad_conexion_bajada}<span data-tr="mb down">mb bajada</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaMóvil;
                //If subrate is equal to 2 show it
                }else if(y.tipo_tarifa == 2){

                    const subtarifaFijo = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/ios/50/000000/phone.png">
                            <p data-tr="Unlimited minutes">Minutos ilimitados</p>
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb up">mb subida</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaFijo;
                //If subrate is equal to 3 show it
                }else if(y.tipo_tarifa == 3){

                    const subtarifaFibra = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/metro/50/000000/network-cable.png">
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb fiber optic rise">mb subida fibra óptica</span></p>
                            <p>${y.subtarifa_velocidad_conexion_bajada}<span data-tr="mb fiber optic down">mb bajada fibra óptica</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaFibra;
                //If subrate is equal to 4 show it
                }else if(y.tipo_tarifa == 4){

                    const subtarifaWifi = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/metro/50/000000/wifi.png">
                            <p>${y.subtarifa_velocidad_conexion_subida}<span data-tr="mb up">mb subida</span></p>
                            <p>${y.subtarifa_velocidad_conexion_bajada}<span data-tr="mb down">mb bajada</span></p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaWifi;
                //If subrate is equal to 5 show it
                }else if(y.tipo_tarifa == 5){

                    const subtarifaTV = `
                        <div class="tarifa--subtarifa">
                            <img src="https://img.icons8.com/ios/50/000000/tv.png">
                            <p>${y.subtarifa_num_canales} <span data-tr="free chanels">canales gratis</span></p>
                            <p data-tr="Free futbol package">Paquete fútbol gratis</p>
                        </div>
                    `;
                    document.getElementById("subtarifas" + x.codtarifa).innerHTML += subtarifaTV;

                }

            });
        });
    }
}

export default tarifasCtrl;