/**
 * LEGAL ADVISE COMPONENT
 */
/**
 * Creates avisoLegalCtrl.
 * @class
 * @returns {controller}
 */
class avisoLegalCtrl {
    constructor(res, mark) {
        console.log("legal" + res);
        this.render(res, mark);
    }

    render(res, mark) {
        const markup = `
            <div class="avisoLegal">
                <div id="avisoLegal" class="avisoLegal--father"></div>
            </div>
        `;
    
        document.getElementById(mark).innerHTML = markup;
    
        res.map(function(x) {
            const avisoLegal = `
                <div class="avisoLegal--child">
                    <h2 class="avisoLegal--child--title">${x.key}</h2>
                    <div class="avisoLegal--content">${x.content}</div>
                </div>
            `;
            
            document.getElementById("avisoLegal").innerHTML += avisoLegal;
        });
    }
}

export default avisoLegalCtrl;