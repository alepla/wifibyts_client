//COOKIES COMPONENT
/**
 * Creates cookiesCtrl.
 * @class
 * @returns {controller}
 */
class cookiesCtrl {
    constructor(res, mark) {
        console.log(res);
        this.render(res, mark);
    }

    render(res, mark) {
        const markup = `
            <div class="cookies">
                <div id="cookies" class="cookies--father"></div>
            </div>
        `;

        document.getElementById(mark).innerHTML = markup;


        res.forEach((x) => {
            const cookies = `
                <div class="cookies--child">
                    <h2 class="cookies--child--title">${x.key}</h2>
                    <div class="cookies--content">${x.content}</div>
                </div>
            `;
            
            document.getElementById("cookies").innerHTML += cookies;
        });
    }
}

export default cookiesCtrl;