/**
 * The slider
 * @function
 * @param {string} texts 
 */
function slider(texts) {
    let allMatches = [];
    //Array to find matches in the texts of the backend and add this into array
    texts.map((x) => {
        let match = x.key.match(/Jumbotron/i);
        if(match){
            allMatches.push(x);
        }
    })
    //Crate an array with the pictures used in the slider
    let images = [
        allMatches[0].image,
        allMatches[1].image,
        allMatches[2].image
    ];

    //Put index to 0
    let imgIndex = 0;
    //Putting the time
    let time = 4000;
    //Hidding the play button
    document.getElementById("play").style.display  = 'none';

    //Loading the defaul views
    addVideoTag(0);

    //The prev button in the slider
    document.getElementById("prev").addEventListener("click", () => {
        prev();
    });
    //The next button in the slider
    document.getElementById("next").addEventListener("click", () => {
        next();
    });

    //The first dot button that load the img tag and the first image and text in the html
    document.getElementById("dotLeft").addEventListener("click", () => {
        imgIndex = 0;
        addVideoTag(imgIndex);
    });
    //The second dot button that load the img tag and the sencs image and text in the html
    document.getElementById("dotCenter").addEventListener("click", () => {
        imgIndex = 1;
        addVideoTag(imgIndex);
    });
    //The third dot button that load the third video tag and the third video and text in the html
    document.getElementById("dotRight").addEventListener("click", () => {
        imgIndex = 2;
        addVideoTag(imgIndex);        
    });

    //The play button to resume the slider, hidding this button and showing the stop button
    document.getElementById("play").addEventListener("click", () => {
        timeout = setInterval(() => {
            if(window.location.href == "http://localhost:8080/#") {
                changeImg();
            }
        }, time);
        document.getElementById("stop").style.display  = '';
        document.getElementById("play").style.display  = 'none';
    });

    //The stop button to stop the slider, hidding this button and showing the play button
    document.getElementById("stop").addEventListener("click", () => {
        clearInterval(timeout);
        document.getElementById("stop").style.display  = 'none';
        document.getElementById("play").style.display  = '';
    });
    
    //The function that change acutocamically the pictures in the slider
    function changeImg() {
        //If it is less than two one is added otherwise imgIndex is equals zero
        if(imgIndex < 2) {
            imgIndex++;
        }else {
            imgIndex = 0;
        }
        //Call the function to know what tag add
        addVideoTag(imgIndex);
    }

    function prev() {
        //If it is equal to 2 it starts again otherwise subtracts one
        if(imgIndex == 0) {
            imgIndex = 2;
        }else {
            imgIndex = imgIndex - 1;
        }
        //Call the function to know what tag add
        addVideoTag(imgIndex);
    }

    function next() {
        //If it is less than two sum one otherwise equals zero
        if(imgIndex < 2) {
            imgIndex = imgIndex + 1;
        }else {
            imgIndex = 0;
        }
        //Call the function to know what tag add
        addVideoTag(imgIndex);
    }
    /**
     * 
     * @param {*} imgIndex 
     */
    function addVideoTag(imgIndex) {
        //If the img ends with mp4 then add the video tag otherwise add the image tag
        if(images[imgIndex].substr(-3) == "mp4") {
            document.getElementById('sliderMedia').innerHTML = '<video class="slider--img" id="slider" src="" autoplay loop>';
        }else {
            document.getElementById('sliderMedia').innerHTML = '<img class="slider--img" id="slider" src="">';
        }
        //Add the following picture and text in the html
        document.getElementById('slider').src = images[imgIndex];
        document.getElementById("sliderText").innerHTML = allMatches[imgIndex].content;
    }

    //Function that runs changeImg every four seconds onfly if the page is home
    let timeout = setInterval(() => {
        if(window.location.href == "http://localhost:8080/#") {
            changeImg();
        }
    }, time);
}

export {slider};