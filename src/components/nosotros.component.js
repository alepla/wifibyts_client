//NOSOTROS COMPONENT
/**
 * Creates nosotrosCtrl.
 * @class
 * @returns {controller}
 */
class nosotrosCtrl {
    constructor(res, mark) {
            console.log(res);
            //Select the lang in localStorage
            let lang = localStorage.getItem("lang");
            let allTexts = [];
            //If lang is null then select the default ES
            if (lang == null) lang = "ES";
            //Filtering the texts by language
            const resultEn = res.filter(text => text.lang.includes("EN"));
            const resultEs = res.filter(text => text.lang.includes("ES"));
            const resultVa = res.filter(text => text.lang.includes("VA"));
            //Sending the data to the template depending the selected language
            if(lang == "ES"){
                this.render(resultEs, mark);
            }else if(lang == "EN"){
                this.render(resultEn, mark);
            }else if(lang == "VA"){
                this.render(resultVa, mark);
            }
    }

    render(res, mark) {
        const markup = `
            <div class="nosotros">
                <h2 class="nosotros--title">Nosotros</h2>
                <div id="nosotros" class="nosotros--father"></div>
            </div>
        `;

        document.getElementById(mark).innerHTML = markup;

        res.map((x) => {
            const nosotros = `
                <div class="nosotros--child">
                    <h2 class="nosotros--child--title">${x.key}</h2>
                    <div class="nosotros--content">${x.content}</div>
                </div>
            `;
            
            document.getElementById("nosotros").innerHTML += nosotros;
        });
    }
}

export default nosotrosCtrl;