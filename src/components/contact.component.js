//CONTACT COMPONENT
import Father from './fatherClass.component';
/**
 * Creates contactCtrl.
 * @class
 * @returns {controller}
 */
class contactCtrl extends Father {
    constructor(res, mark) {
        super(mark);
        this.render(mark);
        //Array with the map coordinates from the backend
        let latLang = {lat: parseFloat(res.location_lat), lng: parseFloat(res.location_long)};
        //Funtion that initliziate the map
        function initMap(latLang) {
            let map = new window.google.maps.Map(document.getElementById('map'), {zoom: 12, center: latLang});
            let marker = new window.google.maps.Marker({position: latLang, map: map});
            marker.setMap(map);
        }
        initMap(latLang);

        //Hidden spans for the errors in the contact form
        document.getElementById("spanName").style.display  = 'none';
        document.getElementById("spanEmail").style.display  = 'none';
        document.getElementById("spanMessage").style.display  = 'none';
        spanSuccess.style.display = 'hidden';
        spanSuccess.style.display = 'none';
        spanError.style.visibility  = 'hidden';
        spanError.style.display = 'none';

        //Checking the errors in the contact form
        document.getElementById("cName").addEventListener("click", ()=> {
            document.getElementById("spanName").style.display  = 'none';
        });

        document.getElementById("cEmail").addEventListener("click", ()=> {
            document.getElementById("spanEmail").style.display  = 'none';
        });

        document.getElementById("cSubject").addEventListener("click", ()=> {
            document.getElementById("spanMessage").style.display  = 'none';
        });

        //AddEventListener for the Contact Button
        document.getElementById("btnContact").addEventListener("click", ()=> {
            let validForm = true;

            //Getting the values from the inputs in the form
            let contactName = document.getElementById('cName').value;
            let contactEmail = document.getElementById('cEmail').value;
            let contactMessage = document.getElementById('cSubject').value;
            
            //Validating the inputs in the form
            if(contactName == null || contactName.length == 0 || /^\s+$/.test(contactName)){
                document.getElementById("spanName").style.display  = '';
                validForm = false;
            } else {
                document.getElementById("spanName").style.display  = 'none';
            }

            if(!(/\S+@\S+\.\S+/.test(contactEmail))){
                document.getElementById("spanEmail").style.display  = '';
                validForm = false;
            } else {
                document.getElementById("spanEmail").style.display  = 'none';
            }

            if(contactMessage == null || contactMessage.length < 50 || contactMessage.length > 150 || /^\s+$/.test(contactMessage)){
                document.getElementById("spanMessage").style.display  = '';
                validForm = false;
            } else {
                document.getElementById("spanMessage").style.display  = 'none';
            }
            //If the form is correct the contact button is disabled and the data is send to the backend
            if(validForm) {

                btnContact.style.visibility  = 'hidden';
                btnContact.style.display = 'none';

                let data = {
                    name: contactName, 
                    email: contactEmail, 
                    message: contactMessage
                };
                spanSuccess.style.visibility  = 'visible';
                spanSuccess.style.display = '';
                console.log(data);

                /*get(Settings.baseURL  + 'contacto/', "POST", data).then((response) => {
                    if(response == true){

                        spanSuccess.style.visibility  = 'visible';
                        spanSuccess.style.display = '';
                        document.getElementById("spanSuccess").innerHTML = "Tu mensaje ha sido enviado correctamente";
                        setTimeout(()=> {
                            window.location="http://localhost:8080/#";
                        }, 3000);
                    }else {

                        spanError.style.visibility  = 'visible';
                        spanError.style.display = '';
                        document.getElementById("spanError").innerHTML = "Ha ocurrido algún error enviando el mensaje";

                        setTimeout(()=> {
                            spanError.style.visibility  = 'hidden';
                            spanError.style.display = 'none';
                            btnContact.style.visibility  = 'visible';
                            btnContact.style.display = '';
                        }, 3000);
                    }
                });*/
            }
        });
    }

    render(mark) {
        const markup = `
            <div class="contact">
                <h2 class="contact--title" data-tr="Contact">Contacto</h2>
                <div class="cotact--body">
                    <div class="contact--form">
                        <h3 class="form--title" data-tr="Contact Us">Contactanos</h3>
                        <form>
                            
                            <span id="spanName" class="spanValidate" data-tr="The name can't be null.">El nombre no puede estar en blanco.</span>
                            <input type="text" id="cName" name="firstname" placeholder="Name" required />

                            <span id="spanEmail" class="spanValidate" data-tr="The email must be valid.">El email debe ser valido.</span>
                            <input type="email" id="cEmail" name="email" placeholder="Email" required pattern="[^@]+@[^@]+\.[a-zA-Z]{2,6}" />

                            <select id="cCountry" name="country">
                                <option value="españa">España</option>
                                <option value="canada">Canada</option>
                                <option value="usa">USA</option>
                            </select>

                            <span id="spanMessage" class="spanValidate" data-tr="The message must be 50 or 150 words.">El mensaje debe tener entre 50 y 150 palabras.</span>
                            <textarea id="cSubject" name="subject" placeholder="Message" style="height:200px" required minlength="50" maxlength="150"></textarea>

                            <input type="button" value="Submit" class="btn" id="btnContact">
                            <span id="spanSuccess" class="spanSuccess" data-tr="Youre message was send correctly">Tu mensaje ha sido enviado correctamente</span>
                            <span id="spanError" class="spanError" data-tr="Something wrong was happened sending the message">Ha ocurrido algún error enviando el mensaje</span>
                        </form>
                    </div>
                    <div class="contact--map">
                        <h3 class="map--title" data-tr="Find Us">Encuentranos</h3>
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        `;

        document.getElementById(mark).innerHTML = markup;
    }
}

export default contactCtrl;