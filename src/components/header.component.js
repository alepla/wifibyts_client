//HEADER COMPONENT
import Father from './fatherClass.component';
import {cambiarIdioma} from '../i18n/i18n';
/**
 * Creates headerCtrl.
 * @class
 * @returns {controller}
 */
class headerCtrl extends Father {
    constructor(res, mark) {
        super(mark);
        this.render(res, mark);

        //Translate in Esp/Val/Eng depending the selected button
        //Spain button
        document
            .getElementById('selectLangEs')
            .addEventListener('click', () => {
                cambiarIdioma('es');
                location.reload();
            });
        //English button
        document
            .getElementById('selectLangEn')
            .addEventListener('click', () => {
                cambiarIdioma('en');
                location.reload();
            });
        //Valencian button
        document
            .getElementById('selectLangVa')
            .addEventListener('click', ()=> {
                cambiarIdioma('vl');
                location.reload();
            });
    }

    render(res, mark) {
        const markup = `
            <div class="lang">
                <button type="button" id="selectLangEn" class="btn--lang" value="EN">EN</button>
                <button type="button" id="selectLangEs" class="btn--lang" value="ES">ES</button>
                <button type="button" id="selectLangVa" class="btn--lang" value="VA">VA</button>
            </div>
            <div>
                <img class="logo" src="${res.logo}" />
                <h2 class="logoTitle">${res.name}</h2>
            </div>
        `;

        document.getElementById(mark).innerHTML = markup;
    }
}

export default headerCtrl;