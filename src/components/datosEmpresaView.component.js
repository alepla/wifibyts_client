//NOSOTROS COMPONENT
import Father from './fatherClass.component';
import {getCookie} from '../utils';
/**
 * Creates nosotrosCtrl.
 * @class
 * @returns {controller}
 */
class datosEmpresaViewCtrl extends Father {
    constructor(res, mark) {
        super(mark);
        //Select the lang in the coockies
        let lang = getCookie("app-lang");
        //If lang is null then select the default ES
        if (lang == null) lang = "es";
        //Filtering the texts by language
        const resultEn = res.filter(text => text.lang.includes("EN") || text.lang.includes("en"));
        const resultEs = res.filter(text => text.lang.includes("ES") || text.lang.includes("es"));
        const resultVa = res.filter(text => text.lang.includes("VA") || text.lang.includes("va"));
        //Sending the data to the template depending the selected language
        if(lang == "es"){
            //If resultEs is diferent to 0 I show the data else show an advise message
            if(resultEs.length != 0){
                this.render(resultEs, mark);
                document.getElementById("messageNoResults").style.display  = 'none';
            }else {
                this.render(resultEs, mark);
                document.getElementById("messageNoResults").style.display  = '';
            }
            
        }else if(lang == "en"){
            //If resultEn is diferent to 0 I show the data else show an advise message
            if(resultEn.length != 0){
                this.render(resultEn, mark);
                document.getElementById("messageNoResults").style.display  = 'none';
            }else {
                this.render(resultEn, mark);
                document.getElementById("messageNoResults").style.display  = '';
            }
        }else if(lang == "vl"){
            //If resultVa is diferent to 0 I show the data else show an advise message
            if(resultVa.length != 0){
                this.render(resultVa, mark);
                document.getElementById("messageNoResults").style.display  = 'none';
            }else {
                this.render(resultVa, mark);
                document.getElementById("messageNoResults").style.display  = '';
            }
        }
    }

    render(res, mark) {
        const markup = `
            <div class="datosEmpresa">
                <p id="messageNoResults" data-tr="No results in that language" class="noResults">No results in that language</p>
                <div id="datosEmpresa" class="datosEmpresa--father"></div>
            </div>
        `;

        document.getElementById(mark).innerHTML = markup;

        res.map((x) => {
            const datosEmpresa = `
                <div class="datosEmpresa--child">
                    <h2 class="datosEmpresa--child--title">${x.key}</h2>
                    <div class="datosEmpresa--content">${x.content}</div>
                </div>
            `;
            
            document.getElementById("datosEmpresa").innerHTML += datosEmpresa;
        });
    }
}

export default datosEmpresaViewCtrl;