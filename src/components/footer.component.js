//FOOTER COMPONENT
import Father from './fatherClass.component';
/**
 * Creates footerCtrl.
 * @class
 * @returns {controller}
 */
class footerCtrl extends Father {
    constructor(res, mark) {
        super(mark);
        this.render(res, mark);
    }

    render(res, mark) {
        const markupFooter = `
            <div class="footer--clear" id="footerClear">
                <div class="footer--clear--left">
                    <a href="#cookies"> Cookies</a>
                    /<a href="#avisoLegal" data-tr="Legal Advise"> Aviso legal</a>
                    /<a href="#nosotros" data-tr="Us"> Nosotros</a>
                    /<a href="#contact" data-tr="Contact"> Contacto</a>
                </div>
                <div class="footer--clear--right">
                    <a href="${res.twitter}"><img class="twitter--btn" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAL0SURBVGhD7dlpqA1hHMfxY1f2pSyRspREpGxlLSElpCReuLZSEuGFF5YkCaW8kRckhUS8kCzlBRLlBZItkmRP2bLv31/nnoxz/+fOnGeemXPU/OrTnTvN85+595x55nmeyWXJkiVLmmmBNvnN/ystsRRn8QG/a33ERaxAR9SXBuie36ybVmiU30ws8/EShYsv5Q30BzVEMLq+2biJsdphZRN25je9RxewC9ZF1+cI9OlMhdo/gfZfgBmd6AV00Brt8JztCF5gHPoK9oGZIQgevBn6HvrINARrx/EF46Honz8ov/k3C1DcaC+aIE4a4w6Ka7t4hFmYjh14hgn4J6tgNb6CXnCNTmTVdfG56PfVqJMlCB4U9A4LUdyDRIk6D6tmXNtgZjKsBkGXMRzl5DysWq5+YR1KpgN+wGocpEInMA5Rcg9WHVfqhkNzElbjUq5D39PeKJVrsNq6UucRmhGI8qlYbmEPFkFdeRfonjoO63hXke9TPT+sAi6+4WvRvjj0IAxN+9qfTXEKVqFKe47QrMcNLIMGY0dhFaskjYxDo++21bia7ENo+sFqXE2WI1JuwypQLYYiUubBKlANNEyK9AxR1Eefg1Wo0g6irHTDA1jFKmkGyk4PXIVVsBJeQc83pzSDni1aBLCKp2kjnKIpbl+0Q09sRXDZJk2foHGbcy7BKpw2/RNjxeeCgavXKIwBY+UYrBOkpQZeovVYTZ6skyRNkzyvaYvTsE6WlMfoBO9RL6aP+T6sE/ukidgwJBoNYUZhLQ7A5+xPvsPpCe4aLVP6XlTQWsFMpJLO2ALNxa2LcfUeU5BodH8MhlZItIBsXUgcDzEAsaMuVsMQDUe6YiAmYTEOIcpLGVe70Rpeoht4DrTSbZ0sCXrbNBGJRKPdlXgK6+Q+3MVcRF5kixO9PNGC9mH4uCd0I+/HGPh6cVR2dP/oftmAM3gL62ILfkKzSw0vNIcYjcjz7LSj0Wh/aJ1Yr8BGQh2EOozmyJIlS9Ull/sDJKNw3QwWZlkAAAAASUVORK5CYII="></a>
                    <a href="${res.facebook}"><img class="facebook--btn" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAIDSURBVGhD7dq/SxdxHMfxr0ttumTNroqULRrUEDgZCP4H1dASririFFJDhhCtNedQ0CQ4SFsJCRIEKahLg0MhYv5oSH2+D0++nu/v3cfjPnfvg88LHsv3Pp8PvPh873vc3bfRIrfxEiv4jeMK/cVPvMUDtCEznXiPI2iLWvAFt9Ay3diENtka2aURXIjsRF1KxP6hH+cyB22wdRu4iijyfbN8TmR5iigz0AbUxVdEkZ9YbUBd/EcHKr9OFEF+cdUDZdjHZ7zGM4wrZqHNTboL9YBPh5hEO7LSC22NpNKLHOAeXGO2yBguE5NFduHydWqOySILuGxMFnmFtPThA5bw7dQPaGsllVpkCmlZhDbPRalFJpCW79DmuQhF8qh1kV+IT9yHSIvcYsdjY3+grZvkvUjWLmTlE7R1k8wXWYa2bpL5IlvQ1k0yXeQKXG/BTRfpgramxnuRecQ3SQNIy2M031S9gbamxnuRZlm7Ey6ICEXyCEUchCJ5hCIOQpE8QhEHoUge3ovIo0ztYNF8FpGHe9HLeO1g0XwWuY7Gu6YPfPJVZBVRhqANKNpHPEkhj460eVmeI4r8t0PejGqDrNvBNZzlJvagDbbsES5kGPJ+T5tg0TRa5g7WoU20YhvqTiQjz5NGIeeNvIzXFqvCGl7g3DnhGvlHQQ/uY7AicrG7gZQ0GicfHdsl7829RAAAAABJRU5ErkJggg=="></a>
                </div>
            </div>
            <div class="footer--dark">
                <p class="footer">©2018 - ${res.name} S.L | ${res.address}, ${res.city} -${res.zipcode}- ( ${res.province} ) ${res.country} | <a href="humans.txt">Alex Pla</a></p>
                <p class="publi">Icons from: <a class="publi" href="https://icons8.com">Icon pack by Icons8</a></p>
            </div>
        `;

        document.getElementById(mark).innerHTML = markupFooter;
    }
}

export default footerCtrl;