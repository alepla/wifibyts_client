//NAVBAR COMPONENT
import Father from './fatherClass.component';
/**
 * Creates navBarCtrl.
 * @class
 * @returns {controller}
 */
class navBarCtrl extends Father {
    constructor(mark) {
        super(mark);
        this.render(mark);
        //Javascript from the burguer menu
        document.getElementById("burguer").addEventListener("click", () => {
            let x = document.getElementById("myTopnav");
            if (x.className === "topnav") {
                x.className += " responsive";
            } else {
                x.className = "topnav";
            }
        });
    }

    render(mark) {
        const markup = `
        <div class="topnav" id="myTopnav">
          <a id="home" data-tr="Home" href="#">Home</a>
          <a id="catalogo" data-tr="Catalogue" href="#catalogo">Catalogue</a>
          <a id="tarifas" data-tr="Rates" href="#tarifas">Rates</a>
          <a id="us" data-tr="Us" href="#nosotros">Us</a>
          <a id="contact" data-tr="Contact" href="#contact">Contact</a>
          <a id="" data-tr="My account" href="">My account</a>
          <a style="font-size:20px;" class="icon" id="burguer">&#9776;</a>
        </div>
        `;

    document.getElementById(mark).innerHTML = markup;
    }
}

export default navBarCtrl;