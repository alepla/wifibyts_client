//CATALOGO COMPONENT
import Father from './fatherClass.component';
/**
 * Creates catalogoCtrl.
 * @class
 * @returns {controller}
 */
class catalogoViewCtrl extends Father {
    constructor(art, filt, fam, mark) {
        super(mark);

        //Declaring the global variables
        let type = [];
        let matches = [];

        //Select the data saved in localStorage
        let catalogo = localStorage.getItem("catalogo");
        let marca = localStorage.getItem("marca");
        let pantalla = localStorage.getItem("pantalla");
        let ram = localStorage.getItem("ram");
        let procesador = localStorage.getItem("procesador");
        let camara = localStorage.getItem("camara");
        //If catalogo is null then select the first element in the array
        if (catalogo == null) catalogo = fam[0].codfamilia;
        //I keep the data of the selected view in type
        fam.forEach((x) => {
            if(x.codfamilia == catalogo) type = x;
        });
        //I keep the elements of the selected view in matches
        art.forEach((x) => {
            if(x.codfamilia == catalogo) {
                matches.push(x);
            }
        });

        //If some element of this is in localstorage then I do a filter of all the products to see if any matches the saved parameter
        if(marca != null) matches = matches.filter(marc => marc.marca == marca);
        if(pantalla != null) matches = matches.filter(pantall => pantall.pantalla == pantalla); 
        if(ram != null)matches = matches.filter(ra => ra.ram == ram);
        if(procesador != null) matches = matches.filter(proces => proces.procesador == procesador);
        if(camara != null)matches = matches.filter(camar => camar.camara == camara);
        
        //Load the default render
        this.render(matches, filt, fam, type, mark);
        document.getElementById("messageNoResults").style.display  = 'none';

        //If the productocs are equal to 0 show the message
        if(matches == 0)document.getElementById("messageNoResults").style.display  = '';
        
        //forEach to chacnge the view depending to the different families of products
        fam.forEach((x) => {
            document.getElementById(x.nombre).addEventListener('click', () => {
                localStorage.setItem('catalogo', x.codfamilia);
                //Here I delete all filters form localStorage
                let filtersToRemove = ["marca", "pantalla", "ram", "procesador", "camara"];
                filtersToRemove.forEach((x) => {
                    localStorage.removeItem(x);
                });
                location.reload();
            });
        });
        //Filter of brand
        document.getElementById('optionsMarca').addEventListener('click', () => {
            //Get the value of the brand selected
            let marca = document.getElementById('optionsMarca').value;
            //If the user select all delete from localStorage else save
            if(marca == "all"){
                localStorage.removeItem('marca');
            }else {
                localStorage.setItem('marca', marca);
            }
            location.reload();
        });
        //Get the value of the inch selected
        document.getElementById('optionsPantalla').addEventListener('click', () => {
            let pantalla = document.getElementById('optionsPantalla').value;
            //If the user select all delete from localStorage else save
            if(pantalla == "all"){
                localStorage.removeItem('pantalla');
            }else {
                localStorage.setItem('pantalla', pantalla);
            }
            location.reload();
        });
        //Get the value of the ram selected
        document.getElementById('optionsRam').addEventListener('click', () => {
            let ram = document.getElementById('optionsRam').value;
            //If the user select all delete from localStorage else save
            if(ram == "all"){
                localStorage.removeItem('ram');
            }else {
                localStorage.setItem('ram', ram);
            }
            location.reload();
        });
        //Get the value of the processor selected
        document.getElementById('optionsProcesador').addEventListener('click', () => {
            let procesador = document.getElementById('optionsProcesador').value;
            //If the user select all delete from localStorage else save
            if(procesador == "all"){
                localStorage.removeItem('procesador');
            }else {
                localStorage.setItem('procesador', procesador);
            }
            location.reload();
        });
        //Get the value of the camera selected
        document.getElementById('optionsCamara').addEventListener('click', () => {
            let camara = document.getElementById('optionsCamara').value;
            //If the user select all delete from localStorage else save
            if(camara == "all"){
                localStorage.removeItem('camara');
            }else {
                localStorage.setItem('camara', camara);
            }
            location.reload();
        });
        //To show and display the filters selected 
        document.getElementById("marcaX").style.display  = 'none';
        document.getElementById("pantallaX").style.display  = 'none';
        document.getElementById("ramX").style.display  = 'none';
        document.getElementById("procesadorX").style.display  = 'none';
        document.getElementById("camaraX").style.display  = 'none';
        if(marca != null) document.getElementById("marcaX").style.display  = '';
        if(pantalla != null) document.getElementById("pantallaX").style.display  = '';
        if(ram != null) document.getElementById("ramX").style.display  = '';
        if(procesador != null) document.getElementById("procesadorX").style.display  = '';
        if(camara != null) document.getElementById("camaraX").style.display  = '';
        //To delete the filter from localStorage
        document.getElementById('marcaX').addEventListener('click', () => {
            document.getElementById("marcaX").style.display  = 'none';
            localStorage.removeItem('marca');
            location.reload();
        });
        //To delete the filter from localStorage
        document.getElementById('pantallaX').addEventListener('click', () => {
            document.getElementById("pantallaX").style.display  = 'none';
            localStorage.removeItem('pantalla');
            location.reload();
        });
        //To delete the filter from localStorage
        document.getElementById('ramX').addEventListener('click', () => {
            document.getElementById("ramX").style.display  = 'none';
            localStorage.removeItem('ram');
            location.reload();
        });
        //To delete the filter from localStorage
        document.getElementById('procesadorX').addEventListener('click', () => {
            document.getElementById("procesadorX").style.display  = 'none';
            localStorage.removeItem('procesador');
            location.reload();
        });
        //To delete the filter from localStorage
        document.getElementById('camaraX').addEventListener('click', () => {
            document.getElementById("camaraX").style.display  = 'none';
            localStorage.removeItem('camara');
            location.reload();
        });
    }

    render(matches, filt, fam, type, mark) {
        //The default html
        const markup = `
            <div class="catalogo">
                <div id="title"></div>
                <p class="catalogo--categorias--title" data-tr="Change category:">Cambiar categoría:</p>
                <div class="catalogo--filter--type" id="familia"></div>
                <div class="catalogo--filter">
                    <div class="filter--marca">
                        <p data-tr="Brand">Marca</p>
                        <select id="optionsMarca">
                            <option value="all" data-tr="All">Todos</option>
                        </select>
                    </div>
                    <div class="filter--pulgadas">
                        <p data-tr="Inches">Pulgadas</p>
                        <select id="optionsPantalla">
                            <option value="all" data-tr="All">Todos</option>
                        </select>
                    </div>
                    <div class="filter--ram">
                        <p>Ram</p>
                        <select id="optionsRam">
                            <option value="all" data-tr="All">Todos</option>
                        </select>
                    </div>
                    <div class="filter--procesador">
                        <p data-tr="Processor">Procesador</p>
                        <select id="optionsProcesador">
                            <option value="all" data-tr="All">Todos</option>
                        </select>
                    </div>
                    <div class="filter--camara">
                        <p data-tr="Camera">Camara</p>
                        <select id="optionsCamara">
                            <option value="all" data-tr="All">Todos</option>
                        </select>
                    </div>
                </div>
                <div class="filter--filters">
                    <div class="filter--filtrosActivated" id="marcaX">
                        <p data-tr="Brand">Marca<p><img class="filter--filtrosActivated--img" src="https://img.icons8.com/metro/50/000000/delete-sign.png">
                    </div>
                    <div class="filter--filtrosActivated" id="pantallaX">
                        <p data-tr="Inches">Pantalla<p><img class="filter--filtrosActivated--img" src="https://img.icons8.com/metro/50/000000/delete-sign.png">
                    </div>
                    <div class="filter--filtrosActivated" id="ramX">
                        <p>Ram<p><img class="filter--filtrosActivated--img" src="https://img.icons8.com/metro/50/000000/delete-sign.png">
                    </div>
                    <div class="filter--filtrosActivated" id="procesadorX">
                        <p data-tr="Processor">Procesador<p><img class="filter--filtrosActivated--img" src="https://img.icons8.com/metro/50/000000/delete-sign.png">
                    </div>
                    <div class="filter--filtrosActivated" id="camaraX">
                        <p data-tr="Camera">Camara<p><img class="filter--filtrosActivated--img" src="https://img.icons8.com/metro/50/000000/delete-sign.png">
                    </div>
                </div><br/>
                <p id="messageNoResults" data-tr="No results" class="noResults">No results</p>
                <div id="smartphones" class="catalogo--father"></div>
            </div>
        `;

        document.getElementById(mark).innerHTML = markup;
        //The headboard
        const title = `
            <div class="catalogo--cabecera">
                <img class="catalogo--cabecera--img" src="${type.imagen_cabecera}" />
                <h2>${type.pretitulo} <br/> ${type.titulo}</h2>
                <h2></h2>
            </div>
        `;
        document.getElementById('title').innerHTML = title;

        //Type of products
        fam.forEach((x) => {
            const familia = `
                <div class="catalogo--smartphone" id="${x.nombre}">
                    <img src="${x.icono}"><br/>
                    <a>${x.nombre}</a>
                </div>
            `;
            document.getElementById("familia").innerHTML += familia;
        })

        //Filters, I add all the options in the selects
        if(filt.marca){
            filt.marca.forEach((x) => {
                const optionsMarca = `
                    <option value="${x.id}">${x.Marca}</option>
                `;
                document.getElementById("optionsMarca").innerHTML += optionsMarca;
            });
        }

        if(filt.pantalla){
            filt.pantalla.forEach((x) => {
                const optionsPantalla = `
                    <option value="${x.id}">${x.num_pantalla} pulgadas</option>
                `;
                document.getElementById("optionsPantalla").innerHTML += optionsPantalla;
            });
        }

        if(filt.ram){
            filt.ram.forEach((x) => {
                const optionsRam = `
                    <option value="${x.id}">${x.num_ram}</option>
                `;
                document.getElementById("optionsRam").innerHTML += optionsRam;
            });
        }

        if(filt.procesador){
            filt.procesador.forEach((x) => {
                const optionsProcesador = `
                    <option value="${x.id}">${x.num_procesador}</option>
                `;
                document.getElementById("optionsProcesador").innerHTML += optionsProcesador;
            });
        }

        if(filt.camara){
            filt.camara.forEach((x) => {
                const optionsCamara = `
                    <option value="${x.id}">${x.num_camara}Mp</option>
                `;
                document.getElementById("optionsCamara").innerHTML += optionsCamara;
            });
        }

        //Products, show all the products
        matches.forEach((x) => {
            const matches = `
                <div class="catalogo--child">
                    <img class="catalogo--img" src="${x.imagen}" />
                    <h2 class="catalogo--titlo"> ${x.descripcion}</h2>
                    <h2 class="catalogo--desc">${x.descripcion_breve}</h2>
                </div>
            `;
            document.getElementById("smartphones").innerHTML += matches;
        });
    }
}

export default catalogoViewCtrl;