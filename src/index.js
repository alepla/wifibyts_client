/**
 * Index.js
 * @param {string} title - Index.js.
 */
//Imports from the index.js
import '../www/css/app.css';
import {Router} from './router.js';
import {get} from './utils';
//Importing the header, footer and navbar
import header from './components/header.component';
import footer from './components/footer.component';
import navBar from './components/navBar.component';
//Importing the components and the views inside this
import HomeController from './components/home.component';
import ContactController from './components/contact.component';
import TarifasController from './components/tarifas.component';
import datosEmpresaViewCtrl from './components/datosEmpresaView.component';
import CatalogoController from './components/catalogo.component'
import {cambiarIdioma} from './i18n/i18n';
//Routes used in the routing
Router
    .add(/contact/, () => {
        get('datos_empresa/').then((response)=> {
            let res = JSON.parse(response);
            new ContactController(res, "main");
            cambiarIdioma();
        });
        localStorage.setItem("view", "http://localhost:8080/#contact");
    }).listen()
    
    .add(/catalogo/, () => {
        get('articulo/').then((articulos)=> {
            let art = JSON.parse(articulos);
            get('filtros/').then((filtros)=> {
                let filt = JSON.parse(filtros);
                get('familia/').then((familia)=> {
                    let fam = JSON.parse(familia)
                    new CatalogoController(art.results, filt, fam.results, "main");
                    //Calling the function to change the lang after load the view
                    cambiarIdioma();
                });
            });
        });
        localStorage.setItem("view", "http://localhost:8080/#catalogo");
    }).listen()

    .add(/tarifas/, () => {
        get('tarifa/').then((response)=> {
            let res = JSON.parse(response);
            new TarifasController(res.results, "main");
            //Calling the function to change the lang after load the view
            cambiarIdioma();
        });
        localStorage.setItem("view", "http://localhost:8080/#tarifas");
    }).listen()

    .add(/nosotros/, () => {
        matches("us");
        localStorage.setItem("view", "http://localhost:8080/#nosotros");
    }).listen()

    .add(/cookies/, () => {
        matches("cookies");
        localStorage.setItem("view", "http://localhost:8080/#cookies");
    }).listen()

    .add(/avisoLegal/, () => {
        matches("legal");
        localStorage.setItem("view", "http://localhost:8080/#avisoLegal");
    }).listen()

    /*.add(/products\/(.*)\/edit\/(.*)/, () => {
        console.log('products', arguments);
    })*/

    .add(()=> {
        home();
        localStorage.removeItem("view");
    });

//Function that load all the info from home
function home() {
    get('home/').then((response)=> {
        let res = JSON.parse(response);
        get('tarifa/?destacado=true').then((tarifas)=> {
            let tar = JSON.parse(tarifas);
            get('datos_empresa/').then((textosEmpresa)=> {
                let texts = JSON.parse(textosEmpresa);
                new HomeController(res, tar.results, texts, "main");
                //Calling the function to change the lang after load the view
                cambiarIdioma();
            });
        });
    });
}
/**
 * 
 * @param {*} search
 * @returns {view} 
 */
function matches(search) {
    get('datos_empresa/').then((response)=> {
        let res = JSON.parse(response);
        let allMatches = [];
        //Array to find matches in the texts of the backend and add this into array
        res.textos.map((x) => {
            let matchCookies = x.content.match(/cookies | Cookies/i) || x.key.match(/cookies | Cookies/i);
            let matchLegal = x.content.match(/legal | legales/i) || x.key.match(/legal | legales/i);
            let matchUs = x.content.match(/Lorem/i);
            if(matchCookies && search == "cookies"){
                allMatches.push(x);
            }else if(matchLegal && search == "legal"){
                allMatches.push(x);
            }else if(matchUs && search == "us"){
                allMatches.push(x);
            }
        });
        new datosEmpresaViewCtrl(allMatches, "main");
        //Calling the function to change the lang after load the view
        cambiarIdioma();
    });
}

//Temporal solution for the routing
let view = localStorage.getItem("view");

if(view){
    window.location.href = view;
}else {
    //Default option loads the Home Controller
    home();
}

document.addEventListener("DOMContentLoaded",() =>{
	
    //Here loads the template of the favicon
    /**
     * 
     * @param {*} res 
     */
    function displayQuote(res) {
        let link = document.createElement('link');
        link.rel = "shortcut icon";
        link.type = "image/png";
        link.href = res.icon_logo;
        document.getElementsByTagName('head')[0].appendChild(link);
    }

    get('datos_empresa/').then((response) => {
        let res = JSON.parse(response);
        displayQuote(res);
        new navBar("navBar");
        new footer(res, "footer");
        new header(res, "header");
        //Calling the function to change the lang after load the view
        cambiarIdioma();
    });
});
