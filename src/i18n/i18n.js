//Import the cookies functions
import {getCookie, setCookie} from '../utils';
//Import the json with the traductions
let frases = require('./languages.json');
/**
 * 
 * @param {String} lang 
 * @function
 */
function cambiarIdioma(lang) {
    //Get the coockie with the lang, default is en
    lang = lang || getCookie('app-lang') || 'en';
    //Save the lang. in the cookies
    setCookie('app-lang', lang, 365);
    //Get all the elements data-tr in tje html
    let elems = document.querySelectorAll('[data-tr]');
    //And here I walk all the elements with data-tr attr and add the transalted text
    elems.forEach((x) => {
        x.innerHTML = frases.hasOwnProperty(lang) ? frases[lang][x.dataset.tr] : x.dataset.tr;
    })
}

export { cambiarIdioma };