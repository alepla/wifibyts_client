'use strict';

import datosEmpresaView from '../../src/components/datosEmpresaView.component.js';
import {setCookie} from '../../src/utils.js';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<div id="main"></div>';

    let text = [
        {key: "texto", lang: "es"},
        {text: "texto", lang: "va"}
    ];
    
    setCookie('app-lang', "es", 365);
    new datosEmpresaView(text, "main");

});

test('Checking if the template from datosEmpresaView loads', () => {
    expect($('#datosEmpresa').attr('class')).toBe('datosEmpresa--father');
});

test('Checking the lang filter', () => {
    //Two for each iteration
    expect($('#datosEmpresa div').length).toBe(2);
    expect($('#messageNoResults').css('display')).toBe('none');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new datosEmpresaView(undefined, "main")}).toThrowError(/undefined/);    
});  