'use strict';

import catalogo from '../../src/components/catalogo.component';
const $ = require('jquery');

beforeEach(()=> {
    document.body.innerHTML = '<div id="main"></div>';

    let res = [
        {codfamilia: 1, lang: "ES"},
        {marca: "text", imagen: "http://asdasd.com"}
    ];

    let fam = [
            {codfamilia: 1}
    ];
    localStorage.setItem('marca', 1);
    new catalogo(res, res, fam, "main");
});

test('Checking if the template from catalogo loads', () => {
    expect($('#smartphones').attr('class')).toBe('catalogo--father');
});

test('Checking the filter', () => {
    expect($('#smartphones div').length).toBe(0);
    expect($('#marcaX').css('display')).toBe('block');
    $("#marcaX").click();
    expect($('#marcaX').css('display')).toBe('none');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new catalogo(undefined, "main")}).toThrowError(/undefined/);    
});  