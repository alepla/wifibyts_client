'use strict';

import contact from '../../src/components/contact.component';
const $ = require('jquery');

// Set up our document body
beforeEach(()=> {
    document.body.innerHTML = 
        '<div id="main"></div>';

        let info = [
            {text: "texto", lang: "ES"},
            {text: "text", lang: "EN"}
        ];

        window.google = {
            maps: {
                Map: class {},
                Marker: class{
                    setMap(){}
                }
            }
        };

        new contact(info, "main");
});

test('Validating the input name', () => {
    document.getElementById("btnContact").click();
    expect(document.getElementById("spanName").innerHTML).toBe('El nombre no puede estar en blanco.');
});

test('Checking if the template from contact loads', () => {
    expect($('#cSubject').attr('name')).toBe('subject');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new contact(undefined, "main")}).toThrowError(/undefined/);    
});  

