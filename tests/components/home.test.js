'use strict';

import home from '../../src/components/home.component.js';
import {setCookie} from '../../src/utils.js';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<div id="main"></div>';

    let res = [
        {text: "texto", lang: "ES"},
        {text: "text", lang: "EN"}
    ];

    let tar = [
        {
            subtarifas: [
                {logo: "http://logo.com"}
            ]
        }
    ];

    let texts = undefined;

    setCookie('app-lang', "es", 365);
    new home(res, tar, texts, "main");

});

test('Checking if the template from home loads', () => {
    expect($('#tarifa').attr('class')).toBe('tarifa--father');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new home(undefined, undefined, undefined, "main")}).toThrowError(/undefined/);    
});  
