'use strict';

import tarifa from '../../src/components/tarifas.component.js';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<div id="main"></div>';

    let res = [
        {
            subtarifas: [
                {logo: "http://logo.com"}
            ]
        }
    ];

    new tarifa(res, "main");

});

test('Checking if the template from tarifas loads', () => {
    expect($("#tarifa").attr('class')).toBe('tarifa--father');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new tarifa(undefined, "main")}).toThrowError(/undefined/);    
});  
