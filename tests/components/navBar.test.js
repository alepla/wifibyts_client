'use strict';

import navBar from '../../src/components/navBar.component.js';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<div id="navBar"></div>';

    new navBar("navBar");

});

test('Checking the burguer button', () => {
    document.getElementById("burguer").click();
    expect($("#myTopnav").attr('class')).toEqual('topnav responsive');
});

test('Checking if the template from navBar loads', () => {
    expect($('#myTopnav').attr('class')).toBe('topnav');
});
