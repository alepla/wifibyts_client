'use strict';

import footer from '../../src/components/footer.component.js';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<div id="footer"></div>';

    let info = [
        {twitter: "http://twitter.com"},
        {facebok: "http://facebook.com"},
        {name: "alvarion"}
    ];

    new footer(info, "footer");

});

test('Checking if the template from footer loads', () => {
    expect($('#footerClear').attr('class')).toBe('footer--clear');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new footer(undefined,"footer")}).toThrowError(/undefined/);    
});  
