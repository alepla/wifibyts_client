'use strict';

import header from '../../src/components/header.component.js';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<div id="header"></div>';

    let info = [
        {logo: "http://logo.png"},
        {name: "http://name.png"}
    ];

    new header(info, "header");

});

test('Checking the lang button En', () => {
    document.getElementById("selectLangEn").click();
    expect(document.getElementById("selectLangEn").value).toEqual('EN');
});

test('Checking the lang button Es', () => {
    document.getElementById("selectLangEs").click();
    expect(document.getElementById("selectLangEs").value).toEqual('ES');
});

test('Checking the lang button VA', () => {
    document.getElementById("selectLangVa").click();
    expect(document.getElementById("selectLangVa").value).toEqual('VA');
});

test('Checking if the template from header loads', () => {
    expect($('#selectLangEn').attr('class')).toBe('btn--lang');
});

test('Component must fail due to JSON input doesnt contains expected information', () => { 
    expect(function(){new header(undefined, "header")}).toThrowError(/undefined/);    
});  
