'use strict';

import {get, setCookie, getCookie} from '../src/utils';

const mockXHR = {
  open: jest.fn(),
  send: jest.fn(),
  status: 200,
  response: JSON.stringify(
      [
          { name: 'aplisein' },
          { logo: 'http://logo.jpg' }
      ]
  )
};
window.XMLHttpRequest = jest.fn(() => mockXHR);

test('Calling the get method', function(done) {
    const reqPromise = get();
    mockXHR.onload();
    reqPromise.then((posts) => {
        let post = JSON.parse(posts);
        expect(post.length).toBe(2);
        expect(post[0].name).toBe('aplisein');
        expect(post[1].logo).toBe('http://logo.jpg');
        done();
    });
});

const mockXHRError = {
    open: jest.fn(),
    send: jest.fn(),
    readyStatus: 404,
    response: "Error 404"
};

test('Calling the get method with error', () => {
    window.XMLHttpRequest = jest.fn(() => mockXHRError);
    const reqPromise = get("","GET");
    mockXHRError.onload();
    reqPromise.then((response) => {
        expect(response.length).toBe(0);
        expect(response).toBe('Error 404');
        done();
    });
});

test('Calling the Cookies functions', () => {
    setCookie('app-lang', "es", 365);
    expect(getCookie('app-lang')).toBe('es');
});