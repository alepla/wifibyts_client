'use strict';

import {Router} from '../src/router.js'; //Knows what to do for every single URL 

it('Router routes add and delete works', () => {
     
    expect(Router.routes.length).toBe(0);
    Router.add(/catalogo/, function() {});
    expect(Router.routes.length).toBe(1);
    Router.add(/tarifas/, function() {});
    expect(Router.routes.length).toBe(2);
    Router.remove(/tarifas/);
    expect(Router.routes.length).toBe(1);
    Router.flush();
    expect(Router.routes.length).toBe(0);
});