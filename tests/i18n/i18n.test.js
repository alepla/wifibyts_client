'use strict';

import {cambiarIdioma} from '../../src/i18n/i18n.js';
import {setCookie} from '../../src/utils';
const $ = require('jquery');

beforeEach(()=> {

    document.body.innerHTML = '<p id="messageNoResults" data-tr="No results in that language" class="noResults">No results in that language</p>';
    setCookie('app-lang', "es", 365);
    cambiarIdioma();

});

test('Checking if the translate function works', () => {
    expect($('#messageNoResults').text()).toBe('No hay resultados en ese idioma');
});
