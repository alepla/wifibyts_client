'use strict';

import { slider } from "../src/components/sliderCtrl";
import {get} from '../src/utils';
const $ = require('jquery');

// Set up our document body
document.body.innerHTML = 
'<div class="slider">'+
    '<div class="slider--text" id="sliderText"></div>'+
    '<div id="sliderMedia"></div>'+
    '<a class="prev" id="prev">&#10094;</a>'+
    '<a class="next" id="next">&#10095;</a>'+
    '<div style="text-align:center">'+
    '<span class="dot " id="dotLeft"></span>'+
    '<span class="dot " id="dotCenter"></span>'+
    '<span class="dot " id="dotRight"></span>'+
    '</div>'+
    '<div style="text-align:center">'+
        '<a class="play" id="play">&#9655;</a>'+
        '<a class="stop" id="stop">&#9632;</a>'+
    '</div>'+
'</div>';


test('Checking the function dotCenter', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("dotCenter").click();
    expect($('#slider').attr('src')).not.toBeNull();
});

test('Checking the function dotLeft', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("dotLeft").click();
    expect($('#slider').attr('src')).not.toBeNull();
});

test('Checking the function dotRight', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("dotRight").click();
    expect($('#slider').attr('src')).not.toBeNull();
});

test('Checking the function prev video', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("prev").click();
    expect($('#sliderMedia').html()).not.toBeNull();
});

test('Checking the function next img', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("next").click();
    expect($('#sliderMedia').html()).not.toBeNull();
});

test('Checking the function play video', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("play").click();
    expect(document.getElementById("stop").style.display).toBe("");
})

test('Checking the function stop video', async () => {
    await get('datos_empresa/').then((response) => {
        let texts = JSON.parse(response);
        slider(texts.textos);
    })
    document.getElementById("stop").click();
    expect(document.getElementById("stop").style.display).toBe("none");
})